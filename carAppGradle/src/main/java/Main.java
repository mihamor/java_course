import car.Car;

public class Main {
    public static void main(String [] args) {

        Car car = new Car();
        car.start();
        System.out.println("Car running: " + car.isEngineOn());
    }
}
