package CarRunner;

public class Car {

    private boolean engineOn;
    public  Car() {
        engineOn = false;
    }

    public void start() {
        this.engineOn = true;
    }

    public boolean isEngineOn() {
        return this.engineOn;
    }
}
